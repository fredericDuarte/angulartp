import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angularTP';
  isAuth = false;
  image_url = ""

  constructor() {
    setTimeout(
      () => {
        this.isAuth = true;
        this.image_url= "http://loga-engineering.com/wp-content/uploads/2019/10/angular-logo.png"
      }, 3000
    );
  }

}
