import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-classroom',
  templateUrl: './classroom.component.html',
  styleUrls: ['./classroom.component.css']
})
export class ClassroomComponent implements OnInit {

  classroomName : String = 'Django et Angular';
  numberStudents: Number = 12

  diisplayStatus(): string {
     return 'Active';
}
  constructor() { }

  ngOnInit(): void {
  }

}
